import { LOGIN_USER,
         LOGOUT_USER,
         ADD_SHARE_USER,
         DELETE_SHARE_USER
    } from '../constants/ActionTypes';

const initialState = {
  email: '',
  name: '',
  shareUsers: [],
  lists: [],
  id: ''
};

export default function users(state = initialState, action) {
  switch (action.type) {
  case LOGIN_USER:
    return {
        email: action.email,
        name: action.name,
        shareUsers: action.shareUsers || [],
        lists: action.lists || [],
        id: action.id
    };

  case LOGOUT_USER:
    return initialState;

  case ADD_SHARE_USER:
    state.shareUsers.push(action.user)
    return state;

  case DELETE_SHARE_USER:
    state.shareUsers = state.shareUsers.filter((user) => {
        return user.id != action.id;
    });
    return state;

  default:
    return state;
  }
}