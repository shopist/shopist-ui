import React, { Component, PropTypes, findDOMNode } from 'react';

export default class Login extends Component {
  constructor(props) {
    super(props);
  }

  getInputValue() {
    return findDOMNode(this.refs.input).value;
  }

  setInputValue(val) {
    findDOMNode(this.refs.input).value = val;
  }

  render() {
    return (
      <div>
      </div>
    );
  }
}

Login.propTypes = {};
