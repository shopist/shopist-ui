import * as types from '../constants/ActionTypes';
import { CALL_API } from '../middleware/api';

export const USER_REQUEST = 'USER_REQUEST';
export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_FAILURE = 'USER_FAILURE';

export function loginUser(email, name, shareUsers, lists, id) {
  return { type: types.LOGIN_USER, email, name, shareUsers, lists, id };
}

export function logoutUser() {
  return { type: types.LOGOUT_USER};
}

export function editTodo(user) {
  return { type: types.ADD_SHARE_USER, user };
}

export function completeTodo(id) {
  return { type: types.DELETE_SHARE_USER, id };
}

function fetchLogin(data) {
  return {
    [CALL_API]: {
      types: [USER_REQUEST, USER_SUCCESS, USER_FAILURE],
      endpoint: '/login',
      type: 'POST',
      data: data
    }
  };
}

export function login(data) {
  return (dispatch) => {
    return dispatch(fetchLogin(data));
  };
}