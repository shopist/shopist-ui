import API_PREFIX from './../constants/Api';

function callApi(endpoint, type, data) {
  const fullUrl = API_PREFIX + endpoint;
  return $.ajax({
        url: fullUrl,
        type: 'POST',
        data: data
    })
    .then(response =>
      response.json().then(json => ({ json, response }))
    ).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      console.info(json)
      return Object.assign({}, json);
    });
}

export const CALL_API = Symbol('Call API');

export default store => next => action => {
  const callAPI = action[CALL_API];
  console.info(action, CALL_API)
  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  let { endpoint, type, data } = callAPI;
  const { types } = callAPI;

  if (typeof endpoint === 'function') {
    endpoint = endpoint(store.getState());
  }

  function actionWith(data) {
    const finalAction = Object.assign({}, action, data);
    delete finalAction[CALL_API];
    return finalAction;
  }

  const [requestType, successType, failureType] = types;

  next(actionWith({ type: requestType }));

  return callApi(endpoint, type, data).then(
    response => next(actionWith({
      response,
      type: successType
    })),
    error => next(actionWith({
      type: failureType,
      error: error.message || 'Something bad happened'
    }))
  );
}

