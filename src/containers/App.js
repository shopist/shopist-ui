import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Login from '../components/Login';
import * as UsersActions from '../actions/users';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { user, dispatch } = this.props;
    const actions = bindActionCreators(UsersActions, dispatch);
    actions.login({
      "email": "test",
      "password": 123
    });
    return (
      <div>
        <Login />
      </div>
    );
  }
}

App.propTypes = {
  user: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

App.contextTypes = {
  history: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps)(App);
